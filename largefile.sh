#!/bin/sh                                                                   
                                                                                
[ -d /home/ubuntu/largefile ] && echo "$(tput setaf 1)Directory Exists$(tput
     sgr 0)" || mkdir /home/ubuntu/largefile                                    
                                                                                
log_file="/home/ubuntu/largefile/largefile.log"                             
                                                                                
#change to / file system                                                    
                                                                                
echo -n "$(tput setaf 2)Enter Directory Path:$(tput sgr 0)"                 
                                                                                
read dir1                                                                   
                                                                                
cd "$dir1"                                                                  
                                                                                
pwd                                                                         
                                                                                
#check large files and displays big files of them                           
                                                                                
find  -type f -size +500M -exec ls -lh {} \; 2>/dev/null | awk '{ print $NF ": " $5 }' 2>&1 | tee $log_file 